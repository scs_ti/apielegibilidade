from datetime import timedelta

from flask import Flask, jsonify, request

from dotenv import load_dotenv

from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager

import cx_Oracle, os

app = Flask(__name__)
load_dotenv()
app.config['JWT_SECRET_KEY'] = os.environ['SECRET_KEY']
jwt = JWTManager(app)

@app.route('/')
def raiz():
    return jsonify({"API": "Elegibilidade"})

@app.route('/isElegivel', methods=['POST'])
def isElegivel():
    try:
        conn = cx_Oracle.connect(os.environ['CONN_STR'])    
    except:
        return jsonify(msg='Erro ao conectar à base de dados'), 503
    
    req = request.json
    cursor = conn.cursor()
    cursor.execute('''SELECT TO_CHAR(u.NR_CPF), 
        pcs.CLASSIFICACAO,
        p.DS_PLANO,
        u.SN_ATIVO, 
        u.NM_SEGURADO, 
        u.NR_TELEFONE, 
        u.TP_SEXO, 
        u.NM_SOCIAL, 
        u.DS_EMAIL, 
        TO_CHAR(u.DT_NASCIMENTO, 'DD/MM/YYYY')
        FROM USUARIO u 
        INNER JOIN PLANO p ON p.CD_PLANO = u.CD_PLANO
        INNER JOIN PLANO_CLASSIFICACAO_SJC pcs ON pcs.CODIGO = u.CD_PLANO 
        WHERE u.NR_CPF = ''' + str(req['cpfBeneficiario']))
    
    data = cursor.fetchone()
    if data == None:
        return jsonify(msg='Beneficiário não consta na base de dados'), 401
    
    payload = {"cpfBeneficiario": data[0], "carteiraPlano": data[1], "produto": data[2], "elegivel": data[3], "nomeBeneficiario": data[4], "telefoneBeneficiario": data[5], "generoBeneficiario": data[6], "nomeSocialBeneficiario": data[7], "emailBeneficiario": data[8], "dataNascimentoBeneficiario": data[9]}
        
    token = create_access_token(identity=payload, expires_delta=timedelta(minutes=1))

    return token, 200

@app.route('/handleToken')
@jwt_required()
def handleToken():
    token = get_jwt()
    return token['sub']

app.run(debug=True)